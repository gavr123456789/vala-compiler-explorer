
void main(){
        try {
            File file = File.new_for_path ("test.vala");
            FileMonitor monitor = file.monitor_file (FileMonitorFlags.NONE, null);
            print ("Monitoring: %s\n", file.get_path ());

            monitor.changed.connect ((src, dest, event) => {
                if (dest != null) {
                    print ("%s: %s, %s\n", event.to_string (), src.get_path (), dest.get_path ());
                   
                    //  if (cmd_status == 0){
                    //      FileUtils.set_contents()
                    //  }
                } else {
                    print ("%s: %s\n", event.to_string (), src.get_path ());
                    string cmd_out;
                    string cmd_err;
                    int cmd_status;
                    Process.spawn_command_line_sync ("valac -C test.vala",
                                    out cmd_out, out cmd_err, out cmd_status);
                }
            });

            new MainLoop ().run ();
        } catch (Error err) {
            print ("Error: %s\n", err.message);
        }
}

[Print]
inline void prin(string str){
    if (str == null) debug("str == null!");
    stdout.printf (str + "\n");
}
