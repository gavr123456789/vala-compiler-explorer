# Vala Compiler Explorer
Vala -> C WYSIWYG Service

# Instruction
$ meson build  
$ ninja -C build  
$ ./build/exec  

Open VSc, File->Auto Save On  
Run program and start edit test.vala

# Features
- [ ] Add commands to the compiler in the runtime
- [ ] Get a list of characters
- [ ] Get a graph of class dependencies
- [ ] Process multiple files at once
- [ ] May be Meson somehow(needdesign)
